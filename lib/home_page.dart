import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:get/get.dart';
import 'package:todoapp/widgets/bottom_sheet.dart';
import 'package:todoapp/controller/home_controller.dart';

class HomePage extends StatefulWidget {
  HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  HomeController homeCt = Get.find();
  final ValueNotifier<bool> isLoading = ValueNotifier<bool>(false);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blue,
          title: Text("ToDoList",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                  color: Colors.white)),
        ),
        body: homeCt.tasks.isNotEmpty
            ? ListView.builder(
                itemCount: homeCt.tasks.length,
                itemBuilder: (BuildContext context, index) {
                  return Padding(
                    padding: const EdgeInsets.all(10),
                    child: Slidable(
                      endActionPane: ActionPane(
                        motion: StretchMotion(),
                        children: [
                          ValueListenableBuilder(
                              valueListenable: isLoading,
                              builder: (BuildContext context, bool value,
                                  Widget? _) {
                                return SlidableAction(
                                  onPressed: (context) {
                                    isLoading.value = true;
                                    showModalBottomSheet(
                                        isScrollControlled: true,
                                        context: context,
                                        builder: (BuildContext context) {
                                          homeCt.controller.text =
                                              homeCt.tasks[index].name!;
                                          return CustomBottomSheet(
                                            title: "Edit Your Task",
                                            buttonText: "UPDATE",
                                            onPressed: () {
                                              setState(() {
                                                homeCt.editTask(index,
                                                    homeCt.controller.text);
                                              });
                                              Get.back();
                                            },
                                          );
                                        }).then((value) {
                                      isLoading.value = false;
                                    });
                                  },
                                  icon: Icons.edit,
                                  foregroundColor: Colors.blue,
                                );
                              }),
                          SlidableAction(
                              onPressed: (context) {
                                setState(() {
                                  homeCt.deleteTask(index);
                                });
                              },
                              icon: Icons.delete_outline,
                              foregroundColor: Colors.red),
                        ],
                      ),
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black.withOpacity(0.3),
                              spreadRadius: 0,
                              blurRadius: 5,
                              offset: Offset(3, 3),
                            ),
                          ],
                        ),
                        child: GetBuilder<HomeController>(
                            init: HomeController(),
                            builder: (homeCt) {
                              return ListTile(
                                  title: Text(
                                    homeCt.tasks[index].name!,
                                    style: TextStyle(fontSize: 18),
                                  ),
                                  leading: Icon(
                                    homeCt.tasks[index].icon,
                                    color: Colors.blue,
                                  ),
                                  trailing: homeCt.tasks[index].icon == null
                                      ? IconButton(
                                          icon: Icon(
                                            Icons.check,
                                            color: Colors.blue,
                                          ),
                                          onPressed: () {
                                            homeCt.toggleTaskCompletion(index);
                                          },
                                        )
                                      : SizedBox());
                            }),
                      ),
                    ),
                  );
                },
              )
            : Center(child: Image.asset("assets/images/task.jpg")),
        floatingActionButton: ValueListenableBuilder(
            valueListenable: isLoading,
            builder: (BuildContext context, bool value, Widget? _) {
              return isLoading.value
                  ? SizedBox()
                  : FloatingActionButton(
                      onPressed: () {
                        showModalBottomSheet(
                            context: context,
                            isScrollControlled: true,
                            builder: (BuildContext context) {
                              homeCt.controller.clear();
                              return CustomBottomSheet(
                                title: "Add Your Task",
                                buttonText: "SAVE",
                                onPressed: () {
                                  setState(() {
                                    if (homeCt.controller.text.isNotEmpty) {
                                      homeCt.addTask();
                                    }
                                  });
                                  Get.back();
                                },
                              );
                            });
                      },
                      child: const Icon(Icons.add),
                    );
            }));
  }
}
