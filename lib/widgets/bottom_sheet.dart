import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:todoapp/controller/home_controller.dart';

class CustomBottomSheet extends StatelessWidget {
  CustomBottomSheet({
    super.key,
    this.onPressed,
    required this.buttonText,
    required this.title,
  });
  final Function()? onPressed;
  final String buttonText;
  final String title;

  @override
  Widget build(BuildContext context) {
    HomeController homeCt = Get.find();
    return SingleChildScrollView(
        child: Container(
      padding:
          EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 25, vertical: 25),
        child: Column(
          children: [
            Text(
              title,
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 25),
            TextFormField(
              controller: homeCt.controller,
              decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5),
                    borderSide: BorderSide(width: 0.2, color: Colors.grey),
                  ),
                  hintText: "Enter The Task"),
            ),
            SizedBox(height: 25),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                OutlinedButton(
                    onPressed: () {
                      Get.back();
                    },
                    child: Text(
                      "CANCEL",
                      style: TextStyle(color: Colors.blue),
                    )),
                ElevatedButton(
                    style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(
                      Colors.blue,
                    )),
                    onPressed: onPressed,
                    child: Text(
                      buttonText,
                      style: TextStyle(color: Colors.white),
                    ))
              ],
            )
          ],
        ),
      ),
    ));
  }
}
