import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomeController extends GetxController {
  TextEditingController controller = TextEditingController();
  final List<Todo> tasks = [];

  void addTask() {
    tasks.add(Todo(name: controller.text, icon: null));
    controller.clear();
    update();
  }

  void editTask(int index, String name) {
    Todo data = tasks.elementAt(index);
    data.name = name;
    update();
  }

  void deleteTask(int index) {
    tasks.removeAt(index);
    update();
  }

  void toggleTaskCompletion(int index) {
    tasks[index].icon = Icons.check_box;
    update();
  }
}

class Todo {
  String? name;
  IconData? icon;
  Todo({this.name, this.icon});
}
